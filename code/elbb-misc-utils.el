;;; elbb-misc-utils.el --- misc-utils for all Emacsen
;; Keywords: lisp

;; This is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; This is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;; Copyright (C) 2009 by Andreas Roehler
;; Author Andreas Roehler  <andreas.roehler@online.de>

(defun read-buffer-by-top-level-form (&optional beg end) 
  "Read buffer step by step by top-level-form. 
Prints position into the message-buffer after evaluation. 
Stepps through region if activated, otherwise takes the whole buffer."
  (interactive)
  (when (string-match ".gnu-emacs-all-cvs" (buffer-file-name)) (emacs-lisp-mode)) 
  (let ((beg (cond (beg beg)
		   ((region-active-p)
		    (region-beginning))
		   (t (point-min)))) 
	(end (cond (end (copy-marker end))
		   ((region-active-p)
		    (copy-marker (region-end)))
		   (t (copy-marker (point-max)))))
        (last-pos-line 1)
        this-pos-line)
    (save-excursion
      (toggle-read-only 1)
      (catch 'fehler
        (goto-char beg)
        (while (and (not (eobp))
                    (< (point) end))
          (setq last-pos-line (count-lines 1 (point)))
          (message "%s" last-pos-line)
          (forward-sexp 1)
          ;; in comment
	  (while (nth 4 (parse-partial-sexp (line-beginning-position) (point)))
	    (forward-line 1)
	    (end-of-line)) 
          (message "point %s" (point))
          (setq this-pos-line (count-lines 1 (point)))
          (if (eq 1 (- this-pos-line last-pos-line))
              (unless (y-or-n-p "Attention: Single line espressions. Continue?")
                (throw 'fehler (ignore)))
            (save-excursion 
              (eval-last-sexp nil))))
        (message "%s" " Finished! Buffer was set `read-only' for security reasons. ")))))

;; (global-set-key [(control kp-9)] 'reverse-chars)
(defun reverse-chars ()
  "Reverse reciproke chars as \"[\" to \"]\", upcase or downcase. "
  (interactive "*")
  (let* ((cf (char-after)) 
         (cn (downcase cf)))
    (cond ((or (eq cf 62)(eq cf ?\>))
           (setq cn "<"))
          ((or (eq cf 60)(eq cf ?\<))
           (setq cn ">"))
          ((or (eq cf 40)(eq cf ?\())
           (setq cn ")"))
          ((or (eq cf 41)(eq cf ?\)))
           (setq cn "("))
          ((or (eq cf 123) (eq cf ?\{))
           (setq cn "}"))
	  ((or (eq cf 125) (eq cf ?\}))
           (setq cn "{"))
          ((or (eq cf 93)(eq cf ?\]))
           (setq cn "["))
          ((or (eq cf 91)(eq cf ?\[))
           (setq cn "]"))
          ((or (eq cf 45)(eq cf ?\-))
           (setq cn "_"))
          ((or (eq cf 95)(eq cf ?\_))
           (setq cn "-"))
          (t (when (eq cf cn)
               (setq cn (upcase cf)))))
    (delete-char 1) 
    (insert cn)))

(require 'thing-at-point-utils)
;; thing-at-point-utils are available at
;; https://code.launchpad.net/s-x-emacs-werkstatt/
(defun peel-list-atpt () 
  "Remove list at point, preserve inner lists. 
Whatever is part of a list at the
beginning and end of the present i.e. sourrounding list
will be deleted.
Must be called from the part of list to remove."
  (interactive "*")
  (save-excursion 
    (let* ((bounds (bounds-of-list-atpt))
           (beg (car bounds))
           (end (cdr bounds)))
      (when (eq beg (point))
        (forward-char 1))
      (down-list)
      (let* ((bounds (bounds-of-list-atpt))
             (inner-beg (car bounds))
             (inner-end (cdr bounds))) 
        (delete-region inner-end end)
        (goto-char beg)
        (delete-region beg inner-beg)))))

;; re-implemented here to run without `thing-at-point-utils'
(require 'thingatpt)
(defun peel-list-at-point ()
  "Remove list at point, preserve inner lists. 
Whatever is part of a list at the
beginning and end of the present i.e. sourrounding list
will be deleted.
Must be called from the part of list to remove."
  (interactive "*")
  (save-excursion 
    (let ((beg (save-excursion (progn (beginning-of-thing 'list) (point)))) 
          (end (save-excursion (progn (end-of-thing 'list) (point)))))
      (when (eq beg (point))
        (forward-char 1))
      (down-list)
      (let ((inner-beg (save-excursion (progn (beginning-of-thing 'list) (point))))
            (inner-end (save-excursion (progn (end-of-thing 'list) (point)))))
        (delete-region inner-end end)
        (goto-char beg)
        (delete-region beg inner-beg)))))

;; elbb-misc-utils.el ends here
