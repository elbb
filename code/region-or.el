;;; region-or.el --- functions dealing with region

;; Keywords: lisp

;; This file is free software; you can redistribute it 
;; and/or modify it under the terms of the GNU General
;; Public License as published by the Free Software
;; Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the
;; implied warranty of MERCHANTABILITY or FITNESS FOR A
;; PARTICULAR PURPOSE.  See the GNU General Public
;; License for more details.

;; You should have received a copy of the GNU General
;; Public License along with GNU Emacs; see the file
;; COPYING.  If not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;; Purpose of this file is to collect as to discuss some
;; small and general forms

;; Everyone interested in Emacs Lisp should easily be
;; able to augment or change this git-archive

;; Instead of marking a region, quite often a default
;; value may do the right thing - thus saving keystrokes.

;; Below an example function: Maybe better methods
;; exist? Comments welcome!

;; Author: Andreas Roehler <andreas.roehler@online.de>

(defun return-region-or-line (&optional arg beg end)
  " "
  (interactive "p")
  (let* ((beg (cond (beg beg)
                    ((region-active-p)
                     (region-beginning))
                    (t (line-beginning-position)))) 
         (end (cond (end (copy-marker end))
                    ((region-active-p)
                     (copy-marker (region-end)))
                    (t (copy-marker (line-end-position)))))
         (text (buffer-substring-no-properties beg end)))
    (when arg (message "%s" text))
    text)) 

;; or as below

;; for (symbol-end-position-atpt) and
;; (symbol-beginning-position-atpt) you need
;; thingatpt-utils from
;; https://code.launchpad.net/s-x-emacs-werkstatt/

;; or replace this form with another one returning
;; beg- end positions

(defun return-region-or-symbol (&optional arg beg end)
  " "
  (interactive "p")
  (let* ((beg (cond (beg beg)
		   ((region-active-p)
		    (region-beginning))
		   (t (symbol-beginning-position-atpt)))) 
	(end (cond (end (copy-marker end))
		   ((region-active-p)
		    (copy-marker (region-end)))
		   (t (copy-marker (symbol-end-position-atpt)))))
        (text (buffer-substring-no-properties beg end)))
    (when arg (message "%s" text))
    text))
    
