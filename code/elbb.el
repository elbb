* Simple useful functions

Date: Thu, 28 Oct 2010 11:56:15 -0700
From: Tak Ota <Takaaki.Ota@am.sony.com>
To: <emacs-devel@gnu.org>
Subject: simple useful functions
Envelope-To: andreas.roehler@online.de

If you think the following two functions are universally useful please
consider incorporating them in simple.el or any appropriate package.
If not disregard.

-Tak

(defun collect-string (regexp &optional num)
  "Collect strings of REGEXP (or optional NUM paren) from the
current buffer into a collection buffer."
  (interactive "sCollect string (regexp): \nP")
  (let ((collection-buffer
	 (get-buffer-create (format "*Collection of \"%s\" *" regexp))))
    (with-current-buffer collection-buffer (erase-buffer))
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward regexp nil t)
	(let ((str (match-string (or num 0))))
	  (if str
	      (with-current-buffer collection-buffer
		(insert str)
		(or (zerop (current-column))
		    (insert "\n")))))))
    (pop-to-buffer collection-buffer)
    (goto-char (point-min))))

(defun source (script &optional shell keep-current-directory)
  "Source the specified shell script.
Source the shell SCRIPT and import the environment into this
emacs.  The optional SHELL specifies the shell other than the
default `shell-file-name'.  When KEEP-CURRENT-DIRECTORY is nil,
which is the default, the current directory is temporarily
changed to the directory where the script resides while sourcing
the script."
  (interactive "fscript file: ")
  (if (null shell)
      (setq shell shell-file-name))
  (with-temp-buffer
    (unless keep-current-directory
      (setq default-directory (file-name-directory script)))
    (call-process shell nil t nil "-c" (concat "source " script "; printenv"))
    (while (re-search-backward "^\\([^=]+\\)=\\(.*\\)$" nil t)
      (setenv (match-string 1) (match-string 2)))))

* How highlight 16 or more occurrences of same character?

To: help-gnu-emacs@gnu.org
From: Oleksandr Gavenko <gavenko@bifit.com.ua>
Date: Mon, 13 Sep 2010 14:20:21 +0300
Subject: Re: How highlight 16 or more occurrences of same character?
Envelope-To: andreas.roehler@easy-emacs.de

On 13.09.2010 11:55, Deniz Dogan wrote:
> 2010/9/13 Oleksandr Gavenko<gavenko@bifit.com.ua>:
>> I use
>>
>> (font-lock-add-keywords
>>   'c-mode
>>   '(
>>    ("\\(.\\)\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1\\1" 0
>> 'font-lock-warning-face t)
>>    ))
>>
>> but don't  know how make variable back links count.
>
> Haven't tested this, but it should be similar to my reply to your
> other thread, with the added comma in {15,}...
>
> (font-lock-add-keywords 'c-mode '(("\\(.\\)\\1\\{15,\\}" 0
> 'font-lock-warning-face t)))
>
Thanks for tip. It work!

* define-key KEYMAP...

From: Miles Bader <miles@gnu.org>
To: Stefan Monnier <monnier@IRO.UMontreal.CA>
Date: Fri, 23 Jul 2010 18:32:55 +0900
Subject: Re: substitute-key-definition vs. define-key MAP [remap ...]

Stefan Monnier <monnier@IRO.UMontreal.CA> writes:
> The `remap' thingy operates at the level of `key-binding'.  I.e. after
> the command loop reads a key-sequence, it looks it up in the keymaps to
> turn the key-sequence into a command, and then it looks this up in the
> `remap' sub-keymap(s) to see if it got remapped.

And it's _extremely_ handy for some uses... e.g., one of my favorite
tricks is little minor-modes that bind [remap self-insert-command]...

E.g.:

   ;;; caps-lock-mode, Miles Bader <miles@gnu.org>

   (defvar caps-lock-mode-map
     (let ((map (make-sparse-keymap)))
       (define-key map [remap self-insert-command] 'self-insert-upcased)
       map))

   (define-minor-mode caps-lock-mode
     "When enabled, convert all self-inserting characters to uppercase."
     :lighter " CapsLock")

   (defun self-insert-upcased (arg)
     (interactive "p")
     (setq last-command-char (upcase last-command-char))
     (self-insert-command arg))

-miles

-- 
Politics, n. A strife of interests masquerading as a contest of
principles. The conduct of public affairs for private advantage.

* LISP-3

To: emacs-devel@gnu.org
From: David Kastrup <dak@gnu.org>
Date: Fri, 23 Jul 2010 12:03:55 +0200
Subject: Re: substitute-key-definition vs. define-key MAP [remap ...]

Stefan Monnier <monnier@IRO.UMontreal.CA> writes:

>> If I understand you correctly, that won't get into XEmacs any time
>> soon.  Do you really mean that `define-key' is allowed to effectively
>> change the command binding of a symbol globally, so that its function
>> definition is ignored in the context of interpreting keystrokes?  Ie,
>> `define-key' now turns Emacs into what is effectively a LISP-3?
>
> I don't know what "ignored in the context of interpreting keystrokes"
> means, nor what LISP-3 means.

I think a LISP-2 has both function and value cells associated with a
symbol (never mind print name and property list in that kind of
accounting), making Scheme a LISP-1.

-- 
David Kastrup

* search-whitespace-regexp

To: help-gnu-emacs@gnu.org
From: Daniel Pittman <daniel@rimspace.net>
Date: Sat, 03 Jul 2010 23:14:59 +1000
Subject: Re: Why does this happen in regexp isearch?

Deniz Dogan <deniz.a.m.dogan@gmail.com> writes:

> I open cus-edit.el.gz in Emacs and do a regexp isearch for:
>
> ^  ;;
>
> (Beginning of line, then two spaces, then two semicolons.)
>
> For some reason that I don't understand, it seems to match "beginning
> of line, any number of spaces OR tabs, then two semicolons". When I
> search for:
>
> ^ \{2\};;
>
> ...it finds what I'm looking for. Why is this?

(defcustom search-whitespace-regexp (purecopy "\\s-+")
  "If non-nil, regular expression to match a sequence of whitespace chars.
This applies to regular expression incremental search.
When you put a space or spaces in the incremental regexp, it stands for
this, unless it is inside of a regexp construct such as [...] or *, + or ?.
You might want to use something like \"[ \\t\\r\\n]+\" instead.
In the Customization buffer, that is `[' followed by a space,
a tab, a carriage return (control-M), a newline, and `]+'.

When this is nil, each space you type matches literally, against one space."
  :type '(choice (const :tag "Find Spaces Literally" nil)
		 regexp)
  :group 'isearch)

Regards,
        Daniel
-- 
✣ Daniel Pittman            ✉ daniel@rimspace.net            ☎ +61 401 155 707
               ♽ made with 100 percent post-consumer electrons

* tabs instead of spaces in C mode

Date: Sun, 2 May 2010 22:13:04 -0400
From: Steve Revilak <steve@srevilak.net>
To: help-gnu-emacs@gnu.org
Subject: Re: Emacs behavior

>From: "VICTOR TARABOLA CORTIANO"=20

>I would like to change the default behavior of Emacs in C mode, I
>use tabs instead of spaces for editing, so I changed my .emacs[1]
>to behave the way I want.
>
>For instance, I want:
>
>function()
>{
>	commands;
>}
>
>But Emacs automatically modify the text to:
>
>function()
>  {
>    commands;
>  }
>
>It works the way I want in text-mode, but I want syntax highligting.
>
>I would like Emacs to behave like mg or vi in C mode.

I've been working on a C project where most of the source code was
indented using tabs, and I wanted my edits to follow the same
formatting.  A solution that worked for me was

/*
  * Local Variables:
  * c-basic-offset: 8
  * indent-tabs-mode: t
  * End:
  */

I added this Local Variables block at the bottom of each .c file I
needed to edit.  (I didn't want to change the behavior of C mode
globally; instead, I merely wanted to change it in a few specific
files.)

Putting something like

  (setq c-basic-offset 8
        indent-tabs-mode t)

into ~/.emacs may give some of the behavior you're looking for.

You can also try

   (setq c-indentation-style "linux")

For a description of cc-mode styles, these are good places to start:

   http://www.gnu.org/software/emacs/manual/html_node/ccmode/Choosing-a-Sty=
le.html#Choosing-a-Style
   http://www.gnu.org/software/emacs/manual/html_node/ccmode/Built_002din-S=
tyles.html#Built_002din-Styles

Finally, c-insert-tab-function might also be useful to you.

    http://www.gnu.org/software/emacs/manual/html_node/ccmode/Indentation-C=
ommands.html#index-TAB-17

Steve

* Using Git to manage your Emacs changes

To: emacs-devel@gnu.org
From: Thierry Volpiatto <thierry.volpiatto@gmail.com>
Date: Fri, 09 Apr 2010 08:04:16 +0200
Subject: Re: Using Git to manage your Emacs changes

John Wiegley <jwiegley@gmail.com> writes:

> On Apr 7, 2010, at 4:37 PM, Thierry Volpiatto wrote:
>
>> Why not using a stack of patchs like stgit or mercurial qpatchs.
>> You can then apply these patchs to bzr repo.
>
> I don't see how stgit improves anything.  Also, I'm using git-bzr
> because I need to fetch the mirrored commits back into Git immediately
> after pushing, and I'm not sure how often the GitHub emacs mirror
> updates itself.

I use http://repo.or.cz/w/emacs.git

This repo is converted to a hg repo locally.
I have cloned this hg repo to another hg repo that handle qpatchs.
So i have three repos:

git, hg, hg qpatch.

1) on git repo: git pull

2) on hg repo: hg convert <last git revision>
   (when the repo exists, hg convert is as fast as a pull)

3) on hg qpatch repo: hg pull

4) make some new patchs on hg qpatch repo (i use DVC and
anything-mercurial.el)

5) Then you can send patchs to Emacs or apply these patchs to bzr repo
directly.(your patchs have to be in git format)

The same can be done with stg.(with only 2 repo)

-- 
Thierry Volpiatto
Gpg key: http://pgp.mit.edu/

* Guile in Emacs (was: integer overflow)

From: Thomas Lord <lord@emf.net>
To: rms@gnu.org
Date: Sun, 11 Apr 2010 16:33:57 -0700
Subject: Re: Guile in Emacs (was: integer overflow)

I'd like to correct some accounts of history.

On Mon, 2010-03-08 at 22:19 -0500, Richard Stallman wrote:
> RS> If it only supports something like Emacs Lisp then it is not much of
>     RS> an advance.  Guile supports Scheme and Javascript as well as Emacs Lisp.
> 
>     It also supports multithreading, which IMHO is a big deal as well,
>     perhaps more important than bignums.
> 
> I think the support for multiple languages is the biggest advance.
> I hope support for other popular extension languages will be
> implemented
> eventually -- that was the original goal of Guile.

Your recollection there seems to me to be mistaken.  It
was a long time ago but I distinctly remember things
differently.  As I recall:

The original GNU Manifesto described a unix-like foundation
with a heavy emphasis on a Lisp-centric user-space.

In the early 1990s, when I worked at the FSF, several
of the hackers there (including me) understood the GNU
vision to imply that many interactive programs - not just
Emacs - would be extensible in a dialect of Lisp.  We
mostly shared the opinion that Scheme was an appropriate
dialect.   

Before I got to the FSF people had already started on a 
GNU Extension Language Library - an embeddable Scheme 
interpreter.   As the (bad) "joke" goes: they code-named the 
project "revoc" but upon reflection that's just a "cover".  
(Get it?  Spooky, ain't it?)

Revoc was (at least back then) going nowhere fast.  Later,
while working on a spreadsheet program, I attempted to start
a new attempt at a scheme-based extension language library.
You were (rightly, I suppose) upset that there were higher
priorities.  You also pointed out that I was probably wasting
time by starting from scratch and that I ought to have started
from some existing Scheme implementation.  I don't recall
if you specifically suggested SCM but I think you might have.
So there were two attempts at a Scheme-based GNU extension
library down - and one to go.

A year or two later I went to work for Cygnus and we, there,
initially agreed to invest in making a scheme-based extension
language library for GNU programs.  This was originally named
"GEL" (for GNU Extension Language) until the company lawyers 
warned that "GEL" was a trademark for another program.  It
was renamed Guile (a cognitive relative of "scheme" and a 
pun for "Guy L.").   You were informed of this work and were
encouraging.   I forget at which stage of this process you 
declared Guile to be a "GNU project" but I'm sure the extant
public record has it somewhere.

Around that time, Sun was beginning to announce and promote 
Java.  They also hired John Ousterhout and began declaring 
"Tcl is to be the scripting language for the Internet!"  Back
then, Sun was a particularly influential company.   On a 
technical level, Tcl was a horribly regressive language - 
a giant step backwards in many subtle but important ways.
Its license was a free software license but it was uncomforable
at the time to have Sun pushing it so heavily because of 
the technical problems and because of doubts about Sun's
motives and control over the software.

A faction arose within Cygnus that argued that Cygnus should
divest from the Guile project and adopt Tcl instead.  I made
you aware of that and of Sun's swagger, regarding Tcl.

Around the same time, two of Prof. Ousterhout's graduate 
students, John Blow and Adam Sah, were proposing that
the TCL language be *altered* in some small but significant
ways that would facilitate its translation into reasonable
efficient Scheme code.  Their altered version of Tcl was
largely but not completely upward compatible with standard
Tcl.  By applying a Scheme->C compiler to the generated 
Scheme code, they were able to benchmark far better than 
standard Tcl in many cases.

And around that same time I was independently proposing
similar things.   Mssrs. Blow, Sah, and I compared notes
and so forth.

Around that time, also, Python was beginning to gain 
recognition in the US (it already had a foothold in Europe).
Blow, Sah, and I had dinner with Von Rossum and tried to 
persuade him to add LAMBDA and proper tail calls and perhaps
continuations to the language so that it could be usefully
translated to Scheme.  He did not agree.  Back at Cygnus,
the oppositional factions had fun lampooning lisp syntax
and pointing to examples like Python of what was better.

Some of my notes to you mysteriously transmogrified into
a USENET post which the archives show as having you in the
"From:" line and with my signature at the bottom of the
message.  This sparked what people, to this day,
still call "The TCL war".

So:

The "original goal" of Guile was most definitely *not*
to support multiple languages.   Rather, that was a goal
that was established as a tactic in response to early
competition with Guile.

Moreover, it was *never*, in the early days, a goal
that Guile support other popular extension languages.
That was something that you added and that I (regrettably)
agreed to in response to the perceived threat of Tcl
and to a lesser extent Python.

And: it was *never* in those years a goal to support
any of those other languages exactly.  It was *always*
a goal to have a Tcl-like syntax, a Python-like syntax,
and a C-like syntax for Guile Scheme.  But tight 
compatibility with those languages was *not* the goal.
The notion wasn't so much to "support Tcl" (or any other
language) as to have a hyper-flexible syntax and to 
support, via libraries, environments with all of the 
convenience features of a shell-like language like Tcl.

Early on after we adopted that tactic you and I and
a few others had some lengthy discussions about adding
Emacs Lisp to the list of languages that Guile could
run.  We ran into some severe issues such as the 
difference between 'NIL in Emacs lisp and '() and Scheme.
Perhaps not to you but to several other of us it became
fairly clear, back then, that a strictly compatible 
Emacs lisp would never sit comfortably alongside a 
proper Scheme environment.  Just as we would need to make
a "Tcl-like" language that would break some Tcl code,
we would need to break some Emacs lisp code, if indeed
we ultimately wanted to bother trying to support any.
(That is part of why, at Cygnus, I built a from-scratch
multi-buffer, self-documenting, extensible text editor
in Scheme with multi-font and proportionally-spaced
font capabilities that, at the time, GNU Emacs wasn't
close to achieving.  The notion was to see how far I could
get just leapfrogging over GNU Emacs instead of trying 
to retrofit it.)

Now, years have passed.  In recent years, I gather, 
the implementation of Guile has been fairly radically
altered so that it is now more of a bytecode VM with
primitive LISP-ish types.   In some sense, it has begun
to drift away from being primarily a Scheme to being
more in the category of JVM or the Mono VM.   It is easier
to target multiple languages to such a VM but no less
easy to make them interoperate cleanly in a way one would 
want to live with for the long run.

So Guile's goals have shifted.  It was once (originally)
to be a tight, clean, fun Scheme environment with some
alternative syntaxes and specialized environments -- and
only later did it become the kind of heterogenous language
environment we see it moving towards today.

So, again:

> I hope support for other popular extension languages will be
> implemented eventually -- that was the original goal of Guile.

Nah.  The heck it was.  That's just not true.

Now, there is a separate question:  is that goal to support
other popular extension languages a better idea or a worse
idea (or not importantly different) than the original goal
of a nice, tight, fun Scheme-based system?

I have my opinion ("far worse!") but it's not my intent
to argue for that opinion here.  Just to set history straight.

That multi-lingual stuff was *not* the original goal of Guile.
On the contrary, the multi-lingual goals only came up at all
because of a perceived crisis sparked by Sun's announcement
that Tcl was to become the ubiquitous scripting language of
the Internet.

-t

* emacs daemon.. but quietly

To: help-gnu-emacs@gnu.org
From: Richard Riley <rileyrgdev@gmail.com>
Date: Mon, 22 Mar 2010 20:02:22 +0100
Subject: Re: emacs daemon.. but quietly

tomas@tuxteam.de writes:

> On Fri, Mar 05, 2010 at 02:24:44PM +0100, Gary . wrote:
>> Is there any way to stop emacs, run with --daemon, printing out
>> details about all of the config files it is loading? At the moment I
>> see
>
> If I understand you correctly, you want to suppress stdout/stderr output
> of emacs --daemon?
>
> You might just redirect that to /dev/null like so:
>
>   emacs --daemon > /dev/null 2>&1
>
> (or did I misunderstand you completely?)
>
>>   ("emacs" "--quiet")
>>   Loading charset...
>>   Loading charset...done
>> (etc.) which is ugly since I want to start the server, when
>> appropriate, when I start my login shell by doing something like
>>
>>   function serverExists {
>>     TMPDIR=${TMPDIR-/tmp};
>>     TMPFILE="${TMPDIR}/ps-output.$$";
>>
>>     ps > ${TMPFILE}
>>     grep -q 'emacs-X11' ${TMPFILE}
>>     SERVER_STARTED=$?;
>>     rm ${TMPFILE}
>>
>>     return $SERVER_STARTED;
>>   }
>>
>>   if serverExists ; then
>>    export EMACS_SERVER="emacs already started"
>>   else
>>    emacs --daemon --quiet
>>    export EMACS_SERVER="emacs started here"
>>   fi
>>   echo $EMACS_SERVER
>>
>> in my .bashrc.
>
> Hm. I don't quite understand this part. Besides, it seems a roundabout
> way. What are you trying to achieve?
>
> Regards
> -- tomás
>

As is emacs --daemon IMO.

In emacs 23 using the alternate editor setting. My "edit" script is:-

,----
| #!/bin/bash
| # edit
| export GDK_NATIVE_WINDOWS=1
| exec emacsclient --alternate-editor="" -c "$@"
`----

Its in the man page for emacsclient and the wiki.

-- 
ASCII ribbon campaign ( )
 - against HTML email  X
             & vCards / \

* Reading a sequence of bytes as one integer

Date: Sat, 13 Mar 2010 20:49:28 +0200
From: Eli Zaretskii <eliz@gnu.org>
To: help-gnu-emacs@gnu.org
Subject: Re: Emacs Lisp - Reading a sequence of bytes as one integer

> Date: Sat, 13 Mar 2010 11:56:17 -0500 (EST)
> From: Jeff Clough <jeff@chaosphere.com>
> 
> I have a (mostly) binary file I want to inspect.  There are several
> places in this file where I want to read a series of bytes (in this
> case, three consecutive bytes) and treat those bytes as a single
> integer.  I can grab them as a buffer-substring obviously, but I'm at
> a loss for how to make that into the integer I need, nor can I find
> anything obvious that would be easier.

Take a look at bindat.el (it's bundled with Emacs).

* how to access a large datastructure efficiently?

To: help-gnu-emacs@gnu.org
From: Thierry Volpiatto <thierry.volpiatto@gmail.com>
Date: Thu, 04 Mar 2010 17:09:35 +0100
Subject: Re: how to access a large datastructure efficiently?

Andreas Röhler <andreas.roehler@easy-emacs.de> writes:

> Thierry Volpiatto wrote:
>> Andreas Röhler <andreas.roehler@easy-emacs.de> writes:
>> 
>>> Thierry Volpiatto wrote:
>>>> Thierry Volpiatto <thierry.volpiatto@gmail.com> writes:
>>>>
>>>>> Hi,
>>>>>
>>>>> Christian Wittern <cwittern@gmail.com> writes:
>>>>>
>>>>>> Hi there,
>>>>>>
>>>>>> Here is the problem I am trying to solve:
>>>>>>
>>>>>> I have a large list of items which I want to access.  The items are in 
>>>>>> sequential order, but many are missing in between, like:
>>>>>>
>>>>>> (1 8 17 23 25 34 45 47 50)  [in reality, there is a value associated 
>>>>>> with this, but I took it out for simplicity]
>>>>>>
>>>>>> Now when I am trying to access with a key that is not in the list, I 
>>>>>> want to have the one with the closest smaller key returned, so for 6 
>>>>>> and 7 this would be 1, but for 8 and 9 this would be 8.
>>>>>>
>>>>>> Since the list will have thousands of elements, I do not want to simply 
>>>>>> loop through it but am looking for better ways to do this in Emacs lisp.  
>>>>>> Any ideas how to achieve this?
>>>>> ,----
>>>>> | (defun closest-elm-in-seq (n seq)
>>>>> |   (let ((pair (loop with elm = n with last-elm
>>>>> |                  for i in seq
>>>>> |                  if (and last-elm (< last-elm elm) (> i elm)) return (list last-elm i)
>>>>> |                  do (setq last-elm i))))
>>>>> |     (if (< (- n (car pair)) (- (cadr pair) n))
>>>>> |         (car pair) (cadr pair))))
>>>>> `----
>>>>>
>>>>> That return the closest, but not the smaller closest, but it should be
>>>>> easy to adapt.
>>>> Case where your element is member of list, return it:
>>>>
>>>> ,----
>>>> | (defun closest-elm-in-seq (n seq)
>>>> |   (let ((pair (loop with elm = n with last-elm
>>>> |                  for i in seq
>>>> |                  if (eq i elm) return (list i)
>>>> |                  else if (and last-elm (< last-elm elm) (> i elm)) return (list last-elm i)
>>>> |                  do (setq last-elm i))))
>>>> |     (if (> (length pair) 1)
>>>> |         (if (< (- n (car pair)) (- (cadr pair) n))
>>>> |             (car pair) (cadr pair))
>>>> |         (car pair))))
>>>> `----
>>>> For the smallest just return the car...
>>>>
>>> if n is member of the seq, maybe equal-operator too
>>>
>>> (<= last-elm elm)
>>>
>>> is correct?
>> 
>> No, in this case:
>> 
>> if (eq i elm) return (list i) ==> (i) ; which is n
>> 
>> and finally (car pair) ==> n
>> 
>
> Hmm, sorry being the imprecise,
> aimed at the first form, whose result equals the the second form once implemented this "="

Ok, i understand, yes, we can do what you say and it's more elegant, i
just notice also i forget to remove a unuseful else:

,----
| (defun closest-elm-in-seq (n seq)
|   (let ((pair (loop with elm = n with last-elm
|                  for i in seq
|                  if (and last-elm (<= last-elm elm) (> i elm)) return (list last-elm i)
|                  do (setq last-elm i))))
|     (if (< (- n (car pair)) (- (cadr pair) n))
|         (car pair) (cadr pair))))
`----

That should work the same.
Thanks. ;-)

-- 
Thierry Volpiatto
Gpg key: http://pgp.mit.edu/

To: help-gnu-emacs@gnu.org
From: Andreas Politz <politza@fh-trier.de>
Date: Thu, 04 Mar 2010 17:49:54 +0100
Subject: Re: how to access a large datastructure efficiently?

[ ... ]

I don't know how hash-table could help in this case, but maybe you want
to consider binary trees, as implemented in the avl-tree.el package.
Though, there is no function for finding the closest member with respect
to some data and a distance-function, but see below.

Finding a (closest) member should be constraint in logarithmic time.

(require 'avl-tree)

(setq avl (avl-tree-create '<))

(dotimes (i 2000)
  (when (= 0 (% i 4))
    (avl-tree-enter avl i)))

(avl-tree-member avl 80)
=> 80
(avl-tree-member avl 70)
=> nil

(defun avl-tree-closest-member (tree data delta-fn)
  ;; delta-fn : data x data -> Z
  (flet ((comp-delta (node)
           (funcall delta-fn data
                    (avl-tree--node-data node))))
    (let* ((node (avl-tree--root tree))
           closest
           (delta most-positive-fixnum)
           (compare-function (avl-tree--cmpfun tree))
           found)
      (while (and node
                  (not found))
        (when (< (comp-delta node) delta)
          (setq delta (comp-delta node)
                closest node))
        (cond
         ((funcall compare-function data (avl-tree--node-data node))
          (setq node (avl-tree--node-left node)))
         ((funcall compare-function (avl-tree--node-data node) data)
          (setq node (avl-tree--node-right node)))
         (t
          (setq found t))))
      (if closest
          (avl-tree--node-data closest)
        nil))))

(mapcar 
 (lambda (data)
   (avl-tree-closest-member
    avl data (lambda (n1 n2)
               (abs (- n1 n2)))))
 '(1001 1002 1003 1004))

=> (1000 1004 1004 1004)

-ap

* cedet and auto-complete

To: help-gnu-emacs@gnu.org
From: Richard Riley <rileyrgdev@gmail.com>
Date: Tue, 02 Mar 2010 08:26:20 +0100
Subject: Re: cedet and auto-complete

Richard Riley <rileyrgdev@gmail.com> writes:

> Just another quick poll to see if anyone has got the new cedet working
> with auto-complete. cedet has developed its own completion UIs but
> ideally I would like to use auto-complete as I do in all other
> modes. Has anyone a workaround or instructions for allowing this to
> work? Unfortunately if you only enable the minimum features which then
> turns off the cedet completion mechs, it also turns off the semantic
> navigation features which kind of detracts from its usefulness.
>
> Any help or pointer appreciated,
>
> r.
>

OK, user errror to a degree - I have now moved back to company-mode
(newer version is available) and made sure it was on the load path
before the version that came with nxhtml.

Works well.

Here is my simple setup which turns on ispell support in text-mode and
so works in gnus message modes.

(add-to-list 'load-path "~/.emacs.d/company-mode")
(require 'company)
(add-hook 'text-mode-hook (lambda()(add-to-list 'company-backends 'company-ispell)))
(require 'company-ispell)
(global-company-mode)

This is with cedet 1.0pre7 and company-mode 0.5

* hide-show squash-minor-modes

From: Thien-Thi Nguyen <ttn@gnuvola.org>
To: Andreas Roehler <andreas.roehler@online.de>
Subject: Re: hideshow condensed view
Date: Thu, 25 Feb 2010 16:21:27 +0100

() Andreas Roehler <andreas.roehler@online.de>
() Thu, 25 Feb 2010 15:24:24 +0100

   thanks a lot maintaining hideshow.el.

In the last few years, i can't take any credit for its improvement.
See Emacs' ChangeLog for those who actually merit the appreciation...

   Use it as default outline-mode now.

Cool.

   Just one thing remains so far: hs displays an empty
   line between headers. Would prefer a more condensed
   view.

You must have good eyes (still); i use hideshow not
for density but for spaciousness.

   Might it be possible to introduce a function
   `hide-all-empty-lines', inclusive a variable which may
   be customized?

Yeah, it would be possible, but i tend to think of empty lines outside
the (top-level) blocks as also outside the scope of hideshow.  It's like
in the book GEB, there is the tree and there is the stuff outside the
tree.  Get it?

I am already troubled by hideshow straying from its conceptual (both
meanings) simplicity -- a block is between matching parens, no more no
less -- but that's just me...

All this is to say, you probably should write a "squash minor mode",
which puts the invisible property on an overlay for text that matches
the empty line regexp (which of course, could be made customizable), at
which point my advice would be to add a hook to `hs-hide-all' to enable
this mode.

Here's a quick (but tested (lightly)) sketch:

 (defvar empty-lines-rx "^\\s-*\\(\n\\s-*\\)+")
 
 (defvar empty-lines nil
   "List of empty lines (overlays).")
 
 (defvar squash-minor-mode nil)
 
 (defun squash-minor-mode () 
   (interactive)
   (setq squash-minor-mode (not squash-minor-mode))
   (if squash-minor-mode
       (save-excursion
         (goto-char (point-min))
         (while (re-search-forward empty-lines-rx nil t)
           (let ((ov (make-overlay (match-beginning 0) (match-end 0))))
             (overlay-put ov 'display "")
             (push ov empty-lines))))
     (mapc 'delete-overlay empty-lines)
     (setq empty-lines nil))
   (message "empty-lines squashing %s" (if squash-minor-mode 'on 'off)))
 
Have fun!

thi

* questioning let

Andreas Roehler <andreas.roehler@online.de> writes:

> Hi,
>
> behaviour of the example code below puzzles me. Would
> expect setting of arg by external function, but inside
> `let', recognised. But remains `1'.
>
> (defun arg-setting ()
>   (interactive)
>   (let ((arg 1))
>     (message "%s" arg)
>     (arg-extern arg)
>     (message "%s" arg)))
>
> (defun arg-extern (arg)
>   (setq arg (1- arg)))
>
> Any help?

From: David Kastrup <dak@gnu.org>
Date: Wed, 24 Feb 2010 19:10:38 +0100
To: help-gnu-emacs@gnu.org
Subject: Re: questioning let

>> The argument binding in arg-extern is the innermost one and consequently
>> the only affected one.  If you make the function argument-less, it will
>> likely work as expected by you, affecting the binding in arg-setting.
>
> That works, thanks a lot!
> However, stored in some eil.el, get a compiler warning than:
>
>
> In arg-extern:
> eil.el:9:9:Warning: reference to free variable `arg'
> eil.el:9:17:Warning: assignment to free variable `arg'
>
> Would think a useless warning, as the compiler should know being inside a let (?)

The warning is completely accurate since arg-extern can be called from
outside arg-setting, in which case it will assign to a global variable
called "arg".

Whether or not some let-binding might be effective at the point of
calling arg-extern is unknown to the compiler.

In a Lisp variant with lexical binding (like Common Lisp or Scheme),
arg-extern has no way to fiddle with the let-binding of arg-setting: it
is completely inaccessible by name outside of arg-setting itself.

-- 
David Kastrup

From: pjb@informatimago.com (Pascal J. Bourguignon)
Date: Wed, 24 Feb 2010 12:59:22 +0100
To: help-gnu-emacs@gnu.org
Subject: Re: questioning let

let is equivalent to lambda:

  (let ((a 1) (b 2)) (list a b)) <=> ((lambda (a b) (list a b)) 1 2)

defun is binding a lambda to a function cell:

  (defun f (a b) (list a b))
       <=> (setf (symbol-function 'f) (lambda (a b) (list a b)))

Therefore you can see that calling a function defined by defun is a let
in disguise.

If you transformed your code following these equivalences, you would
notice that you have actually TWO variables named arg, one as parameter
of the function arg-extern, and one as variable in the let in
arg-setting.

The setq in arg-extern will modify only the variable parameter of
arg-extern.  Because they have the same name, this variable hides the
one defined in the let of arg-setting.  There's no way to access it from
within arg-extern.

If they had a different name, you could modify a variable from an outer
dynamic scope from an inner dynamic scope, because in emacs all the
variables are dynamic.  But it is considered very bad form to do so:
this is a big side effect, and what's more, one that depends on the call
chain.  You should avoid side effects, to increase the readability and
debugability of your code.  Therefore you should avoid setq and setf.
Try to write pure function, never try to modify a variable.

One way to write your code would be:

(defun do-what-you-need-to-do-with (arg)
   )

(defun arg-binding ()
  (interactive)
  (let ((arg 1))
     (message "before arg = %s" arg)
     (let ((arg (arg-extern arg)))
       (message "after arg = %s" arg)
       (do-what-you-need-to-do-with arg))
     (message "original arg = %s" arg)))

(defun arg-extern (arg)
   (message "arg-extern before arg = %s" arg)
   (message "arg-extern returns = %s" (1- arg))
   (1- arg))

before arg = 1
arg-extern before arg = 1
arg-extern returns = 0
after arg = 0
original arg = 1

If you need a global variable (perhaps because you need to keep some
data across command invocations), the I would advise to distringuish it
from the other by giving it a name surrounded by stars: *var*.  Then, it
will have a different name, and won't be shadowed (inadvertantly) by
inner lets, defuns or lambdas.

(defvar *var* 42)

(defun arg-extern (arg)
   (message "arg-extern before arg = %s" arg)
   (setf *var* (1- arg))
   (message "arg-extern returns = %s" *var*)
   *var*)

(arg-binding)
 var*  --> 0

-- 
__Pascal Bourguignon__

* real tab completion in shell-mode

To: help-gnu-emacs@gnu.org
From: Andreas Politz <politza@fh-trier.de>
Date: Tue, 23 Feb 2010 19:43:58 +0100
Subject: Re: real tab completion in shell-mode

Nathaniel Flath <flat0103@gmail.com> writes:

> Hello,
> Is there any way to get shell mode to use the background shell's
> (zsh or bash) tab completion as opposed to just tab-completing
> filenames?  I know about ansi-term, but I hate how it steals a lot
> of my keybindings and so I would prefer it if I could get this to
> work in shell-mode.

There is not really a clean way to implement this for different reasons.

>
> Thanks,
> Nathaniel Flath

A while ago, I did it anyway and wrote a special mode for bash-shells
which features

+ emulation of bash completion
+ dir-tracking (local and remote)
+ tracking of history files
+ displaying dir stack in header line

I took the opportunity to comment the code somewhat, you may give it
a try, if you dare:

http://www.fh-trier.de/~politza/emacs/bash-mode-1_0.tgz

Extract it somewhere in your load-path and read the commentary, or just
M-x bash RET

-ap

* reduce repeated backslashes

From: Teemu Likonen <tlikonen@iki.fi>
Date: Mon, 15 Feb 2010 10:51:28 +0200
To: help-gnu-emacs@gnu.org
Subject: Re: reduce repeated backslashes

  2010-02-15 09:40 (+0100), Andreas Roehler wrote:

> don't know how to replace/reduce repeated backslashes from inside a
> string.
>
> Let's assume "abcdef\\\\"

What is the end result you want? I guess:

    (replace-regexp-in-string "\\\\\\\\" "\\" "abcdef\\\\" nil t)
    => "abcdef\\"

There are 8 backslashes in the regexp string because backslash is a meta
character in Lisp strings and also in regexps. 8 backslashes in a regexp
Lisp string means 2 literal backslashes. In the resulting string there
is only one backslash but it is displayed as two "\\" because it's a
printed representation of Lisp string.

* Why can't I use xargs emacs?

From: pjb@informatimago.com (Pascal J. Bourguignon)
Date: Tue, 02 Feb 2010 23:40:30 +0100
To: help-gnu-emacs@gnu.org
Subject: Re: Why can't I use xargs emacs?

Adam Funk <a24061@ducksburg.com> writes:

> The emacs command can take a list of filename arguments, so why can't
> I get xargs to work with it?
>
> $ find -name '*.txt' |xargs emacs -nw
> emacs: standard input is not a tty
>
> $ grep -rl 'foo' some/path  |xargs emacs -nw
> emacs: standard input is not a tty

emacs is an interactive program.  It expects its stdin and stdout to
be hooked to the terminal, where it can display a character matrix,
and from which it can read user input.

When you use a pipe to send paths to xargs, you disconnect the
terminal from the stdin, and replace it with a pipe.  When xargs forks
and exec emacs, emacs inherit this pipe as stdin, and cannot get user
input, but will get instead further path from grep.

% echo hello > file ; ( echo -b ; echo file ) | xargs -L 1 cat 
hello

To open several files in emacs, you could either use emacsclient, or
an emacs lisp script.

Launch emacs in a separate terminal: xterm -e emacs -nw & 
In emacs, start the server:          M-x server-start RET
In a shell, you can then type:       find -name '*.txt' | xargs emacsclient -n

Simplier would be to just open the file in emacs:

Launch emacs:  emacs -nw 
Then type: C-x C-f *.txt RET

For the second case, you could type:
M-: (map nil 'find-file (split-string (shell-command-to-string "grep -rl 'foo' some/path") "\n")) RET

-- 
__Pascal Bourguignon__

From: Harald Hanche-Olsen <hanche@math.ntnu.no>
Date: Tue, 02 Feb 2010 18:51:39 -0500
To: help-gnu-emacs@gnu.org
Subject: Re: Why can't I use xargs emacs?

+ Adam Funk <a24061@ducksburg.com>:

> The emacs command can take a list of filename arguments, so why can't
> I get xargs to work with it?
>
> $ find -name '*.txt' |xargs emacs -nw
> emacs: standard input is not a tty

$ find -name '*.txt' |xargs sh -c 'emacs -nw "$@" </dev/tty' -

(untested)

-- 
  Harald Hanche-Olsen     <URL:http://www.math.ntnu.no/~hanche/>
- It is undesirable to believe a proposition
  when there is no ground whatsoever for supposing it is true.
  -- Bertrand Russell

To: help-gnu-emacs@gnu.org
From: Thierry Volpiatto <thierry.volpiatto@gmail.com>
Date: Wed, 03 Feb 2010 08:23:52 +0100
Subject: Re: Why can't I use xargs emacs?

Why not a simple:

emacs -nw -Q $(find . -name '*.txt')

Why do you want to use xargs?

Bill Marcum <marcumbill@bellsouth.net> writes:

> ["Followup-To:" header set to comp.unix.shell.]
> On 2010-02-02, Adam Funk <a24061@ducksburg.com> wrote:
>> The emacs command can take a list of filename arguments, so why can't
>> I get xargs to work with it?
>>
>> $ find -name '*.txt' |xargs emacs -nw
>> emacs: standard input is not a tty
>>
>> $ grep -rl 'foo' some/path  |xargs emacs -nw
>> emacs: standard input is not a tty
>>
> It says: standard input is not a tty. I don't normally use emacs, so there
> may be a better way to do this, but you could write a function:
> my_emacs () { emacs "$@" </dev/tty >&0 2>&0 ; }
>
>

-- 
Thierry Volpiatto

From: Adam Funk <a24061@ducksburg.com>
Date: Wed, 03 Feb 2010 14:18:58 +0000
To: help-gnu-emacs@gnu.org
Subject: Re: Why can't I use xargs emacs?

On 2010-02-02, Bit Twister wrote:

> On Tue, 02 Feb 2010 20:22:17 +0000, Adam Funk wrote:
>> The emacs command can take a list of filename arguments, so why can't
>> I get xargs to work with it?
>>
>> $ find -name '*.txt' |xargs emacs -nw
>> emacs: standard input is not a tty
>>
>> $ grep -rl 'foo' some/path  |xargs emacs -nw
>> emacs: standard input is not a tty
>
>
> Maybe it's the -nw switch. Try
>       find -name '*.txt' |xargs emacs

Yes, it works without -nw, but I'm often logged into an ssh server so
it's faster to open emacs in the xterm.

Anyway, the solution (posted by Thierry in gnu.emacs.help) turns out
to be this:

emacs -nw $(find . -name '*.txt')

Thanks.

-- 
..the reason why so many professional artists drink a lot is not
necessarily very much to do with the artistic temperament, etc.  It is
simply that they can afford to, because they can normally take a large
part of a day off to deal with the ravages.          [Amis _On Drink_]

* getting emacs from launchpad

From: Lennart Borgman <lennart.borgman@gmail.com>
Date: Tue, 26 Jan 2010 22:46:47 +0100
To: henry atting <nsmp_01@online.de>
Subject: Re: bazaar question

On Tue, Jan 26, 2010 at 10:31 PM, henry atting <nsmp_01@online.de> wrote:
> I find it annoying that I have to grapple with bazaar if I
> want to go on building emacs from source. Do I really have to pull
> the whole emacs tree starting from the first published sources in the
> Roman Empire?
>
> Tried it with `bazaar branch http://bzr.savannah.gnu.org/r/emacs/trunk/'
> but stopped, unpatiently, after 20 minutes.
> Is this the only way to get the latest source?

You can get them from Launchpad too. That is faster since they have
installed the bazaar fast server (or what it is called).

   bzr branch lp:emacs trunk

After that updates are quick (in the trunk subdir):

  bzr update

* debug modification of a variable

From: Tassilo Horn <tassilo@member.fsf.org>
To: emacs-devel@gnu.org
Date: Tue, 26 Jan 2010 21:26:49 +0100
Subject: Re: How to debug modification to a variable value?

"alin.s" <alinsoar@voila.fr> writes:

> I suggest you to use the WATCH in gdb for modifications of the
> variable, AWATCH for reading of the variable, etc.
>
> To detect the location of the variable is easy: install a breakpoint
> in make-variable-buffer-local that stops when exactly the variable you
> are interested about is set, then see there the location of
> tg-schema-alist.  Afterward you can use the x* functions from .gdbinit
> of emacs to debug your problem...

Thanks for that explanation.  I'll try that out as soon as I find some
time.  And also thanks for volunteering on implementing a debug facility
for cases like that.

Bye,
Tassilo

* strip mail head to fit for elbb

(defun mail2elbb (&optional beg end)
  " "
  (interactive "*")
  (let ((beg (cond (beg beg)
                   ((region-active-p)
                    (region-beginning))
                   (t (point-min))))
        (end (cond (end end)
                   ((region-active-p)
                    (copy-marker (region-end)))
                   (t (point-max)))))
    (save-restriction
      (narrow-to-region beg end)
      (goto-char beg)
      (mail2elbb-intern beg end)
      (widen))))

(defun mail2elbb-intern (beg end)
  (while (search-forward "=A0" nil t 1)
    (replace-match ""))
  (goto-char beg)
  (let ((end (progn (while
                 (re-search-forward "^[[:alpha:]-]+To:" nil t 1) 't) (copy-marker (line-end-position)))))
    (goto-char beg)
    (beginning-of-line)
    (while
        (< (point) end)
      (if (looking-at "Subject:\\|From:\\|Date:\\|To:")
          (forward-line 1)
        (delete-region (line-beginning-position) (1+ (line-end-position)))))))

* testing configuration

From: pjb@informatimago.com (Pascal J. Bourguignon)
Date: Sun, 27 Dec 2009 18:53:08 +0100
To: help-gnu-emacs@gnu.org
Subject: Re: testing configuration

andrea <andrea.crotti.0@gmail.com> writes:

> I was wondering if it would be possible to automatically check if my
> emacs configuration is correct.
> This can be then put in a post-commit hook to check that whenever I add
> some new features or change I don't mess up somethin else
>
> I would only need to:
> - load my whole configuration
> - exit and returns 0 if everything goes fine, returns another number
> otherwise.
>
> Any ideas?
>
> I can already launch a new emacs with certain conf, but how can I get a
> return value?

What about:

emacs -q --eval '(condition-case err (progn (load "~/.emacs") (kill-emacs 0)) (error (kill-emacs 1)))'

?

-- 
__Pascal Bourguignon__                     http://www.informatimago.com/

* self-typing

From: pjb@informatimago.com (Pascal J. Bourguignon)
Date: Sat, 26 Dec 2009 14:32:16 +0100
To: help-gnu-emacs@gnu.org
Subject: Re: what is `self-typing' ?

waterloo <waterloo2005@gmail.com> writes:

> I can not understand a para in Elisp manual :
>
>     Lisp is unlike many other languages in that its objects are
>     "self-typing": the primitive type of each object is implicit in the
>     object itself.  For example, if an object is a vector, nothing can
>     treat it as a number; Lisp knows it is a vector, not a number.
>
> What is `self-typing' ? thanks

As mentionned by Eli.  But this require some more explaination.

Some languages are defined so that variables can hold only objects of
a given type (you declare the type of the variable, or it is infered
automatically at compilation time).  For example, C or Haskell.  

Since all the types of the objects are known at compilation time from
the variables they're stored in, the compiler doesn't need to generate
code to store the type along with the object, or along with the
variable: the type is implicity.  In C, typeof(42) or int a=42; typeof(a)
is computed at compilation time.  

Some languages are defined so that variables can hold objects of
different types, at different times.  In that case, the type is not
attached to the variable, but must be attached to the objects
themselves.  For example, Lisp or Smalltalk.

Since it is rarely possible to know what type of object a variable
will hold (some type inference can still be applied to optimize out
some function, but there remains a lot of functions where type
inference doesn't restrict much the possible types, the more so when
global analysis is not practical or possible), then the type of each
object has to be kept with the object.  In Lisp, (type-of 42) or (let
((a 42)) (type-of 42)) is computed at run-time.  Notably, you can
write: (type-of (read)) and depending on what you enter at run-time,
will get back one type or another:

M-x ielm RET
*** Welcome to IELM ***  Type (describe-mode) for help.
ELISP> (type-of (read))
Lisp expression: 42
integer
ELISP> (type-of (read))
Lisp expression: "fourty-two"
string
ELISP> (type-of (read))
Lisp expression: fourty-two
symbol
ELISP> (type-of (read))
Lisp expression: (fourty two)
cons
ELISP> 

There are also languages where both things are more or less possible,
you can have variables with pre-defined types holding only one type of
object, and variables which can hold objects of various types, but of
a same root class.  For example, C++ or Java.

These languages usually provide so called "Plain Old Data" types which
correspond to the type of objects that can only be stored in variables
with predefined types.   These objects of these POD types usually
don't have the type attached to them, these objects can only be stored
in variables with pre-defined type.   When you want to store such an
object in a variable-type variable, you have to wrap it in another
object, a typed object, instance of a class.  In Java, you have a POD
type int and an object class Integer.  

-- 
__Pascal Bourguignon__                     http://www.informatimago.com/

* Emacs uptime

From: pjb@informatimago.com (Pascal J. Bourguignon)
Date: Wed, 23 Dec 2009 20:16:03 +0100
To: help-gnu-emacs@gnu.org
Subject: Re: Some functions I hope are useful for others to

[ ... ]

For example, here is my emacs-uptime:

(defvar com.informatimago.time/*emacs-start-time*   (current-time)
   "For (emacs-uptime)")

(defun com.informatimago.time/emacs-uptime ()
  "Gives Emacs' uptime, based on global var `com.informatimago.time/*emacs-start-time*'."
  (interactive)
  (let* ((st com.informatimago.time/*emacs-start-time*)
         (cur (current-time))
         (hi-diff (- (car cur) (car st)))
         (tot-sec (+ (ash hi-diff 16) (- (cadr cur) (cadr st))))
         (days (/ tot-sec (* 60 60 24)))
         (hrs  (/ (- tot-sec (* days 60 60 24)) (* 60 60)))
         (mins (/ (- tot-sec (* days 60 60 24) (* hrs 60 60)) 60))
         (secs (/ (- tot-sec (* days 60 60 24) (* hrs 60 60) (* mins 60)) 1)))
    (message "Up %dd %dh %dm %ds (%s), %d buffers, %d files"
             days hrs mins secs
             (format-time-string "%a %Y-%m-%d %T" st)
             (length (buffer-list))
             (count t (buffer-list)
                    :test-not
                    (lambda (ignore buf)
                      (null (cdr (assoc 'buffer-file-truename
                                        (buffer-local-variables buf)))))))))

(defalias 'emacs-uptime 'com.informatimago.time/emacs-uptime)

[ .... ]

* Using Emacs Lisp for script writing

To: help-gnu-emacs@gnu.org
From: David Engster <deng@randomsample.de>
Date: Sat, 19 Dec 2009 11:02:22 +0100
Subject: Re: Using Emacs Lisp for script writing

Andreas Politz <politza@fh-trier.de> writes:
> Cecil Westerhof <Cecil@decebal.nl> writes:
>
>> I already use 'emacs -batch' for scripting where no user input is used,
>> but I would like to use it also for interactive scripting. Until now I
>> did not find any usable information about this. Anybody using Emacs for
>> interactive scripts?
>
> No, but it should be noted, that this is not very difficult :
>
> $ emacs -Q -batch -eval '(yes-or-no-p "Want some cookies ?")'

You can also use the '--script' option in a shebang line:

----------------- test.sh -----------------
#!/usr/bin/emacs --script

(if (yes-or-no-p "Choose ")
    (message "You said yes")
  (message "You said no"))
-------------------------------------------

I use emacs regularly for writing scripts, since with its thousands of
packages you have a huge library readily available.

-David

* until found

From: 	Helmut Eller
Subject: 	Re: until-found
Date: 	Fri, 11 Dec 2009 17:10:38 +0100
User-agent: 	Gnus/5.13 (Gnus v5.13) Emacs/23.1.50 (gnu/linux)

* Andreas Roehler [2009-12-10 13:50+0100] writes:

> And here my implementation so far:
>
> (defun until-found (search-string liste)
>   (let ((liste liste) element)
>     (while liste
>       (if (member search-string (car liste))
>           (setq element (car liste) liste nil))
>       (setq liste (cdr liste)))
>     element))

This seems to be the same as:

(car (member (lambda (element) (member search-string element)) liste))

or

(find-if (lambda (element) (member search-string element)) liste)

or

(find search-string liste :test #'member)

or

(loop for e in liste if (member search-string e) return e)

Helmut

* favourite directories implementation

Author: Florent Georges, source:
http://fgeorges.blogspot.com/2008/01/emacs-favourite-directories.html

Today, I have finally taken a look at one of the
simple features I always missed in Emacs: the
ability to define a set of "favourite directories."
That is, a set of named directories that one can use
in the minibuffer when prompted for instance to open
a file. Given a set of such dirs:

emacs-src -> /enter/your/path/to/emacs/sources
projects  -> /path/to/some/company/projects
now       -> @projects/the/project/I/am/working/on

one can use the following path in the minibuffer to open a file,
for instance using C-x C-f:

@emacs-src/lisp/files.el
@emacs-src/src/alloc.c
@projects/great/README
@now/src/some/stuff.txt

Doing so, completion is available for both directory names and
files under their target directories. For instance, to open the
third file above, you only have to type:

C-x C-f @ p <tab> g <tab> R <tab> <enter>

The implementation I have just written is really simple, but
useful yet. It implements all described above (including
                                               recursive defined directories, as the '@now' above.) Thanks to
Emacs, I am still suprised by the facility to implement such a
feature!

The code was written on GNU Emacs 22.1 on Windows, but should
work on any platform, and I think on Emacs 21 as well.

TODO: Make a custom variable.
(defvar drkm-fav:favourite-directories-alist
  '(("saxon-src"  . "y:/Saxon/saxon-resources9-0-0-1/source/net/sf/saxon")
    ("kernow-src" . "~/xslt/kernow/svn-2007-09-29/kernow/trunk/src/net/sf/kernow"))
  "See `drkm-fav:handler'.")

(defvar drkm-fav::fav-dirs-re
  ;; TODO: Is tehre really no other way (than mapcar) to get the list
  ;; of the keys of an alist?!?
  (concat
   "^@"
   (regexp-opt
    (mapcar 'car drkm-fav:favourite-directories-alist)
    t))
  "Internal variable that stores a regex computed from
`drkm-fav:favourite-directories-alist'.  WARNING: This is not
updated automatically if the later variable is changed.")

(defun drkm-fav:handler (primitive &rest args)
  "Magic handler for favourite directories.

With this handler installed into `file-name-handler-alist', it is
possible to use shortcuts for often used directories.  It uses
the mapping in the alist `drkm-fav:favourite-directories-alist'.

Once installed, say you have the following alist in the mapping
variable:

    ((\"dir-1\" . \"~/some/real/dir\")
     (\"dir-2\" . \"c:/other/dir/for/windows/users\"))

You can now use \"@dir-1\" while opening a file with C-x C-f for
instance, with completion for the abbreviation names themselves
as well as for files under the target directory."
  (cond
   ;; expand-file-name
   ((and (eq primitive 'expand-file-name)
         (string-match drkm-fav::fav-dirs-re (car args)))
    (replace-match
     (cdr (assoc (match-string 1 (car args))
                 drkm-fav:favourite-directories-alist))
     t t (car args)))
   ;; file-name-completion
   ((and (eq primitive 'file-name-completion)
         (string-match "^@\\([^/]*\\)$" (car args)))
    (let ((compl (try-completion
                  (match-string 1 (car args))
                  drkm-fav:favourite-directories-alist)))
      (cond ((eq t compl)
             (concat "@" (match-string 1 (car args)) "/"))
            ((not compl)
             nil)
            (t
             (concat "@" compl)))))
   ;; file-name-all-completions
   ((and (eq primitive 'file-name-all-completions)
         (string-match "^@\\([^/]*\\)$" (car args)))
    (all-completions
     (match-string 1 (car args))
     drkm-fav:favourite-directories-alist))
   ;; Handle any primitive we don't know about (from the info node
   ;; (info "(elisp)Magic File Names")).
   (t (let ((inhibit-file-name-handlers
             (cons 'drkm-fav:handler
                   (and (eq inhibit-file-name-operation primitive)
                        inhibit-file-name-handlers)))
            (inhibit-file-name-operation primitive))
        (apply primitive args)))))

;; Actually plug the feature into Emacs.
(push '("\\`@" . drkm-fav:handler) file-name-handler-alist)

* lisp interface to ispell

From: Teemu Likonen <tlikonen@iki.fi>
Date: Fri, 06 Nov 2009 22:05:53 +0200
To: help-gnu-emacs@gnu.org
Subject: Re: lisp interface to ispell ?

On 2009-11-06 20:39 (+0100), Andreas Politz wrote:

> Does someone have a hack, or know a different package, in order to allow
> elisp access to spelling functions ? E.g. like
>
> (spell word language)
>
> which at least returns t or nil.

Something like this?

(defun my-ispell-string (word lang)
  (with-temp-buffer
    (insert word)
    (call-process-region (point-min) (point-max)
                         "ispell" t t nil "-l" "-d" lang)
    (if (= (point-min) (point-max))
        t)))

* Python workflow

From: Simon <bbbscarter@gmail.com>
Date: Fri, 6 Nov 2009 03:42:44 -0800 (PST)
To: help-gnu-emacs@gnu.org
Subject: Python workflow

Hi, apologies in advance for a potentially numpty post.

I've been using Emacs for a little while now, but I've yet to settle
on a satisfactory python edit-run-debug cycle, and I was wondering
what wiser minds than mine have settled upon. So far I've tried:

- Edit code and run with emacs PDB. After fiddling the lisp code to
automatically pick up the current buffer as the default run candidate,
this is nearly okay. The main issue is that, after editing code,
there's no easy way to rerun the code, so I end up killing the gud
buffer every time. As such, entering and leaving the debugger is quite
a few key presses and accidentally leaving it running is also easy.

- Tried Pydb to similar effect.

- Run everything in a seperate shell. And debug by hand. This is a
little too low-fi, even for me.

- Use the 'import/reload file' and 'eval def/class' functions, and run
everything from the emacs python shell, using pdbtrack to help with
debugging. Problems so far:
- It's very easy to forget which modules you've modified and fail to
reload them; because the state is carried over I continually find
myself running the old versions of code I've just edited, especially
if it's across several files.
- More than that, sometimes the stuff I expect to reload simply
doesn't, and I have no indication as to why. For example, if pdb has
module open and you stick a deliberate error in the code and reload
it, the minibuffer tells you the module has been loaded, even though
it clearly can't have been.
- I have to run pdb.pm() to debug the exception. If I run *anything*
else by accident, I lose the exception context. This can be annoying
if you're incompetent enough to keep making typos (I am).

Does anyone have any tips on their workflow?

Many thanks!

Simon

* strip out UTF-8 BOMs
From: "Edward O'Connor" <hober0@gmail.com>
Date: Thu, 5 Nov 2009 16:13:27 -0800
To: emacs-devel@gnu.org
Subject: find-file-literally-at-point

Hi,

I recently found myself in need of such a function (I was going through
                                                      a bunch of files to strip out their UTF-8 BOMs, if you're curious), and
it was quick enough to put together:

(autoload 'ffap-guesser "ffap")
(defun find-file-literally-at-point ()
  "Open the file at point (like `ffap') with `find-file-literally'."
  (interactive)
  (find-file-literally (ffap-guesser)))

* xml and n3

From: "Eric Schulte" <schulte.eric@gmail.com>
Subject: Re: [Orgmode] org-babel-tangle xml text
Date: Tue, 03 Nov 2009 09:18:34 -0700

"Martin G. Skjæveland" <martige@ifi.uio.no> writes:

> Is there a way I can add xml and n3 to the list of supported
> languages? These languages does not need interpretation, so I'm
> thinking it should be quite easy to add. I have fumblingly tried
>
>   (add-to-list 'org-babel-tangle-langs '("xml"))
>
> and
>
>   (add-to-list 'org-babel-tangle-langs '("css" "xml"))
>
> but it as no effect.
>

Hi Martin,

The attached org-mode file contains instructions for adding xml and n3
to org-babel and org-babel-tangle.  Note that there may be another step
if the major mode for n3 is not n3-mode.  Best -- Eric

introduce org-babel to =xml= and =n3=

#+begin_src emacs-lisp :results silent
  (org-babel-add-interpreter "xml")
  (org-babel-add-interpreter "n3")
#+end_src

if say =n3= should be edited using =xml-mode=, then evaluate the
following adding this pair to =org-src-lang-modes=

#+begin_src emacs-lisp :results silent
  (add-to-list 'org-src-lang-modes '("n3" . xml))
#+end_src

;; inform org-babel-tangle of their existence and file extensions
#+begin_src emacs-lisp :results silent
  (add-to-list 'org-babel-tangle-langs '("xml" "xml" nil t))
  (add-to-list 'org-babel-tangle-langs '("n3" "n3" nil t))
#+end_src

#+begin_src xml :tangle example
  <first>
  </first>
#+end_src

#+begin_src n3 :tangle example
  n3 stuff
#+end_src

* How to check regexp for syntax-errors?

From: Kevin Rodgers <kevin.d.rodgers@gmail.com>
Date: Tue, 20 Oct 2009 01:54:56 -0600
Subject: Re: How to check regexp for syntax-errors?  
David Combs wrote:
> Isn't there some .el that that will try to parse a regexp, and tell
> me where it got confused, and what to look for for errors?
                                                            
(defun valid-regexp-p (regexp)
   (interactive "sRegexp: ")
   (with-temp-buffer
     (condition-case error-data
	(progn
	  (re-search-forward regexp nil t)
	  t)
       (invalid-regexp
        (when (interactive-p) (message "Invalid regexp: %s" (cdr error-data)))
        nil))))

-- 
Kevin Rodgers
Denver, Colorado, USA

* prefer cond over case?

From: David Kastrup <dak@gnu.org>
Date: Mon, 12 Oct 2009 10:03:39 +0200
To: help-gnu-emacs@gnu.org
Subject: Re: Perferr cond over case?

pjb@informatimago.com (Pascal J. Bourguignon) writes:

> Nordlöw <per.nordlow@gmail.com> writes:
>
>> Does the use of the cl macro case() incurr some loss of performance
>> compare to using cond() instead?
>
> (macroexpand '(case (* 2 2 2 2 3)
                  >                 (10      'one)
                  >                 ((24 42) 'two)
                  >                 ((3 33)  'three)
                  >                 (otherwise 'unknown)))
> -->
> (let ((--cl-var-- (* 2 2 2 2 3)))
    >     (cond ((eql --cl-var-- (quote 10)) (quote one))
                >           ((member* --cl-var-- (quote (24 42))) (quote two))
                >           ((member* --cl-var-- (quote (3 33))) (quote three))
                >           (t (quote unknown))))
>
> What do you think?

Before or after byte compilation?

-- 
David Kastrup

* batch-mode

From: Decebal <cldwesterhof@gmail.com>
Newsgroups: gnu.emacs.help
Date: Sat, 10 Oct 2009 11:33:17 -0700 (PDT)
To: help-gnu-emacs@gnu.org
Envelope-To: andreas.roehler@easy-emacs.de

In a Bash script I changed:

local i
local length=${#1}

       for i in $(seq ${1}) ; do
         printf "       Regel %${length}d voor de test\n" ${i}
       done  >${2}
to:

emacs -batch -nw --eval='
        (let (
              (i)
              (nr-of-lines '${1}')
              (nr-of-lines-length)
              (output-file "'"${2}"'"))
          (setq nr-of-lines-length (length (number-to-string nr-of-lines)))
          (dotimes (i nr-of-lines t)
           (insert (format (format "       Regel %%%dd voor de test\n" nr-of-lines-length) (1+ i))))
          (write-file output-file))
       ' 2>/dev/null

The Bash version took 293 seconds and the Emacs Lisp version 59
seconds. So it is about 5 times as fast.
The Emacs batch gives output like:
Saving file /home/cecil/temp/inputEmacs...
Wrote /home/cecil/temp/inputEmacs
That is why I use the 2>/dev/null.
Is there a way to circumvent the generation of the above output?
Because when there is an error it is also thrown away and that is not
nice.

From: Vassil Nikolov <vnikolov@pobox.com>
Newsgroups: gnu.emacs.help
Date: Sat, 10 Oct 2009 16:55:31 -0400
To: help-gnu-emacs@gnu.org
Envelope-To: andreas.roehler@easy-emacs.de

On Sat, 10 Oct 2009 11:33:17 -0700 (PDT), Decebal <cldwesterhof@gmail.com> said:
> ...
>       local i
>       local length=${#1}
>       for i in $(seq ${1}) ; do
>         printf "       Regel %${length}d voor de test\n" ${i}
>       done  >${2}

translates to

emacs -Q -batch -eval '
(dotimes (i '"${1}"')
        (princ (format "       Regel %'"${#1}"'d voor de test\n" (1+ i))))
' > "${2}"

---Vassil.

From: Decebal <cldwesterhof@gmail.com>
Newsgroups: gnu.emacs.help
Date: Sat, 10 Oct 2009 13:57:07 -0700 (PDT)
To: help-gnu-emacs@gnu.org
Envelope-To: andreas.roehler@easy-emacs.de

On Oct 10, 10:06pm, Andreas R=F6hler <andreas.roeh...@easy-emacs.de>
wrote:
> > The Emacs batch gives output like:
> >   Saving file /home/cecil/temp/inputEmacs...
>
> it's in files.el, save-buffer, AFAIS
>
>   (if (and modp (buffer-file-name))
        >     (message "Saving file %s..." (buffer-file-name)))
>
> commenting out these lines should cancel the message

The problem with that is that it only works for me. But I found a way.
I replaced:

(write-file output-file))

with:

(set-visited-file-name output-file)
    (basic-save-buffer))

But maybe there should be more consideration for the possibility that
Emacs is used as a batch program.

> >   Wrote /home/cecil/temp/inputEmacs

I still have to find something for this.

That is not possible I am afraid. In the C-source there is a call to
message_with_string.

* vectors and lists

From: pjb@informatimago.com (Pascal J. Bourguignon)
Newsgroups: gnu.emacs.help
Date: Fri, 09 Oct 2009 19:19:49 +0200
To: help-gnu-emacs@gnu.org
Envelope-To: andreas.roehler@easy-emacs.de

Nordlöw <per.nordlow@gmail.com> writes:

> If I have an association list say,
>
> '(
>   ("key" sym1 val1 num1)
>   ("key2" sym2 val2 num2)
> )
>
> , where each entry is a fixed sequence of various objects. 

If this is an a-list, then you could write it as:

   (("key1" . (sym1 val1 num1))
    ("key2" . (sym2 val2 numb2)))

to show that it is a list of cons cells.  

(a . (b c d)) <=> (a b c d), but the first notation shows that you
consider it as a list of cons, and notably that you don't expect nil
ie. () to be in the toplevel of the a-list.

Also, if we write the a-list properly like this, we can better answer
the following question:

> I might
> aswell use a vector to represent an entry in this alist, right?

You cannot use a vector instead of the cons cells of the a-list, but
you can use a vector as a value of an a-list entry.  Values can be of
any type.  In the case of emacs lisp, you could also easily use
vectors (or any other type) as keys in an a-list, since it uses equal
to compare keys.  

   (("key1" . [sym1 val1 num1])
    ("key2" . [sym2 val2 num2])
    ([?k ?e ?y ?3] . [sym3 val3 num3]))

> In this case, what do I gain by using a vector instead of list?

In general, vectors take half the space of lists, and access to the
nth element is done in O(1) instead of O(n) with lists.  However,
adding or removing an element in a vector is O(n) while in the case of
lists, it may be O(1) (prepending an element or removing the first
                                  element or one of the few firsts elements) or O(n) (inserting,
                                                                                      appending an element or removing the nth element).

> What about performance?: aref() faster than nth() only for large
> vectors?

aref has to compute a multiplication and a sum, before doing one
memory load to get the element.  In the case of emacs lisp, the
multiplication is always by the same fixed factor AFAIK.

nth has to do n memory loads to get the element.

So indeed, aref will probably be faster than nth, even for indices as
small as 1 or 0.

> Is there vector-variant of assoc()? 

No.  Unless you count hash-tables as a vector variant.

> If not, why? 

Because there's no point. The advantage of using a list for a-list,
apart from the historical simplicity, is that you can easily prepend
the a-list with new associations, and therefore use the a-list in a
purely functional way.

   (defun f (bindings)
      (let ((val (cdr (assoc 'x bindings))))
         (if (zerop val)
             (list val)
             (cons val (f (cons (cons 'x (1- val)) bindings))))))

   (let ((bindings '((y . 0) (x . 1))))
      (list (f (cons (cons 'x 2) bindings))
            (cdr (assoc 'x bindings))))
   ;; --> ((2 1 0) 1)

Note: you could use (require 'cl)  (acons key value a-list) 
instead of (cons (cons key value) a-list).

> Has any one already written such a function?

Not AFAIK, but you can write it.  However, the semantics of assoc
require a sequential search of the keys in the list, so there would be
no gain.  On the contrary, we would now have O(n) complexity to
prepend a new entry to the a-vector.

-- 
__Pascal Bourguignon__
