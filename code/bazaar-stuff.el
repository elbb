* commit from vc in Emacs

> Óscar Fuentes skrev:
>> Jan Djärv <jan.h.d@swipnet.se> writes:
>>
>>> Maybe I'm missing something obvious, but I can't find any way of doing
>>> commit from vc in Emacs (for bzr).
>>>
>>> I did what "Doing Quick Fixes" says in BzrForEmacsDevs.  I then did
>>> C-x v d ..., and then v, <type comment> C-c C-c.
>>>
>>> Bzr then complains it can't do single file commits for merges
>>> (whatever that means).
>>
>> Let me guess:
>>
>> bzr merge
>> <hack hack>
>> bzr commit foo.c
>> <cannot do single file commits for merges>
>
> No.  I used vc from inside Emacs.  vc did a commit according to
> .bzr-log.  I never commited a single file.

Well, I was trying to describe on terms of bzr commands what you VC did
at your request. After reading your other messages, I guess that this is
what happened:

cd trunk
bzr merge ../other-branch
<in VC-dir, mark the modified files>
<commit from VC-dir>

As mentioned on my response to Jason, it seems that bzr does not admit a
`commit' comand with a list of files when there is a pending merge. The
solution for this is to not mark files in VC-dir, but put the point on
the `./' entry at the top and press `v' there. This commits all changes
on the current working tree, which keeps bzr happy.

* Emacs available from Launchpad too

From: Lennart Borgman <lennart.borgman@gmail.com>
Date: Tue, 26 Jan 2010 22:46:47 +0100
To: henry atting <nsmp_01@online.de>
Subject: Re: bazaar question

On Tue, Jan 26, 2010 at 10:31 PM, henry atting <nsmp_01@online.de> wrote:
> I find it annoying that I have to grapple with bazaar if I
> want to go on building emacs from source. Do I really have to pull
> the whole emacs tree starting from the first published sources in the
> Roman Empire?
>
> Tried it with `bazaar branch http://bzr.savannah.gnu.org/r/emacs/trunk/'
> but stopped, unpatiently, after 20 minutes.
> Is this the only way to get the latest source?

You can get them from Launchpad too. That is faster since they have
installed the bazaar fast server (or what it is called).

  bzr branch lp:emacs trunk

After that updates are quick (in the trunk subdir):

  bzr update

* merge --uncommitted

On Fri, Jan 8, 2010 at 10:24, Eli Zaretskii <eliz@gnu.org> wrote:

> > Btw, can anyone explain what does "merge --uncommitted" do?  Its
> > description in the docs is cryptic:
> >
> >  --uncommitted    Apply uncommitted changes from a working copy, instead
> >                   of branch changes.

After

  cd trunk
  bzr merge --uncommitted ../my-branch

the trunk's tree will have the changes that were (and still are) in
my-branch's tree. It's a way to copy uncommitted changes between
trees, instead of the normal merge, which copies committed changes
between branches.

    Juanma

On Fri, Jan 8, 2010 at 13:31, Eli Zaretskii <eliz@gnu.org> wrote:

> > So it's a way of merging with modified files from the named branch,
> > without naming those files explicitly?

Yes.

    Juanma
