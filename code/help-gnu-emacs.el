;;; help-gnu-emacs.el --- examples from help-gnu-emacs@gnu.org

;; Copyright (C) 2009  contributors to help-gnu-emacs@gnu.org

;; Author: authors of the contributions
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains stuff found at
;; help-gnu-emacs@gnu.org, which was seems of interest
;; to preserve that way

;; Please note: code here does not reflect fully
;; discussions but delivers an extract from it. As
;; functions may be defined differently, its not
;; suitable to compile it.


;;; Code:


;; Partition for Emacs Lisp
;; Marc Tfardy, bum@cyk.cyk, 28.06.2009 15:26
;; 
;; Hi!
;; 
;; I looking for a ELISP function that do the job like Partition in
;; Mathematica. The simplest case:
;; 
;; (partition '(a b c d e f) 2)
;; 
;; should return:
;; ((a b) (c d) (e f))
;; 
;; Is there something ready out from the box? 
;; 
;; Re: Partition for Emacs Lisp
;; Pascal J. Bourguignon, pjb@informatimago.com, 28.06.2009 19:52

(defun partition-list (list length)
  (loop
     while list
     collect (subseq list 0 length)
     do (setf list (nthcdr length list))))

(defun partition-vector (vector length)
  (loop
     for i = 0 then (+ i length)
     while (< i (length vector))
     collect (subseq vector i (+ i length))))
     
(defun partition (sequence length)
   (etypecase sequence
      (list   (partition-list sequence length))
      (string (partition-vector sequence length)) ; emacs lisp strings are not vectors! 
      (vector (partition-vector sequence length))))

;; (partition '[a b c d e f] 2)  -> ([a b] [c d] [e f])
;; (partition '(a b c d e f) 2)  -> ((a b) (c d) (e f))
;; (partition '"abcdef" 2)       -> ("ab" "cd" "ef")

;; Re: Partition for Emacs Lisp
;; Vassil Nikolov, vnikolov@pobox.com, 02:46

(when (and (fboundp 'featurep) (featurep 'emacs))  ;seen in comp.lang.lisp
  (require 'cl))

(defun partition (l n)  ;"demo" grade, cursorily tested
  "Return a list of L's consecutive sublists of length N."
  (assert (zerop (mod (length l) n)))
  (loop for l on l by #'(lambda (l) (nthcdr n l)) collect (subseq l 0 n)))
  
;; (partition '(a b c d e f) 2)
;; => ((a b) (c d) (e f))

(provide 'help-gnu-emacs)
;;; help-gnu-emacs.el ends here


