* How do I get my (uninstalled) Xemacs to see (uninstalled) packages?

From: Didier Verna <didier@xemacs.org>
To: Alan Mackenzie <acm@muc.de>
Subject: Re: How do I get my (uninstalled) Xemacs to see (uninstalled)
Date: Wed, 24 Mar 2010 10:16:14 +0100

Alan Mackenzie <acm@muc.de> wrote:

> How do I tell this XEmacs where to look to find packages? Which
> variable must I set, and is there anything more to do? I've tried, but
> failed, to find this info in the XEmacs and Lispref manuals. Is it
> there, and if so, where?

This is not exactly the answer you're expecting, but I prefer to do it
that way:

- I do install the packages, but only perform a symlink installation, so
  nothing is really duplicated,
- this also has the advantage that you can remove the symlinks you don't
  want (for instance if you have conflicting versions in site-packages)
  without affecting your copy of the repo.

From: "Stephen J. Turnbull" <stephen@xemacs.org>
To: Alan Mackenzie <acm@muc.de>
Subject: How do I get my (uninstalled) Xemacs to see (uninstalled) packages?
Date: Wed, 24 Mar 2010 20:20:03 +0900

[ ... ]

Documentation is in the node "(xemacs)Startup Paths".

Date: Thu, 25 Mar 2010 13:31:54 -0400
Subject: Re: How do I get my (uninstalled) Xemacs to see (uninstalled) 
From: Vin Shelton <acs@alumni.princeton.edu>
To: Alan Mackenzie <acm@muc.de>

Hello, Alan -

[ ... ]
>
>> Does setting the environment variable
>
>> EMACSLATEPACKAGES=3D~/emacs/packages
>
>> before starting XEmacs help? Reading the lispref manual Package
>> Overview/The User View seems to indicate that it might do what you
>> need.
>
> No, it doesn't help at all. There surely must be some internal variabl=
es
> which reflect all these external attempts to connect with a packages
> directory. Would somebody please tell me what they are.

After poking around for awhile and conducting some experiments, I
don't think XEmacs supports running the packages directly from the CVS
hierarchy.  You will have to create the forest of symlinks that Didier
mentioned earlier:

cd ~/emacs/packages/xemacs-packages
mkdir lisp

cd lisp
ln -s ../c-support

Repeat the final 2 steps as necessary.  If you set

symlink =3D t

in Local.rules, this may happen automatically.  I do not fully grok
that mechanixm.

Mike Sperber - please correct me where I have published incorrect informati=
on.

  - Vin


Date: Thu, 25 Mar 2010 15:32:06 -0500
Subject: Re: How do I get my (uninstalled) Xemacs to see (uninstalled) 
From: Ben Wing <ben@666.com>
To: Vin Shelton <acs@alumni.princeton.edu>

[ ... ]

I think Vin is right.  Alan, why don't you just modify
XEMACS_INSTALLED_PACKAGES_ROOT in Local.rules to some location or
other, then do `make install' from the packages directory.  Then FAQ
question 2.1.1 should tell you how to configure XEmacs to find them.


Date: Thu, 25 Mar 2010 20:35:26 +0000
To: Vin Shelton <acs@alumni.princeton.edu>
Subject: Re: How do I get my (uninstalled) Xemacs to see (uninstalled)
From: Alan Mackenzie <acm@muc.de>

Hi, Vin,

On Thu, Mar 25, 2010 at 01:31:54PM -0400, Vin Shelton wrote:

> After poking around for awhile and conducting some experiments, I don't
> think XEmacs supports running the packages directly from the CVS
> hierarchy.  You will have to create the forest of symlinks that Didier
> mentioned earlier:

> cd ~/emacs/packages/xemacs-packages
> mkdir lisp

> cd lisp
> ln -s ../c-support

> Repeat the final 2 steps as necessary.

Actually, 
$ for f in ../* ; do ln -s $f ${f##*/} ; done
did the job nicely.  :-)

And my XEmacs now starts up with its packages.  :-)  Thank you indeed!

> If you set

> symlink = t

> in Local.rules, this may happen automatically.  I do not fully grok
> that mechanism.

I think I've got that set in my Local.rules, but because of the ((natnump
-1)) problem, I couldn't get that far.

> Mike Sperber - please correct me where I have published incorrect information.

>   - Vin

-- 
Alan Mackenzie (Nuremberg, Germany).

_______________________________________________
XEmacs-Beta mailing list
XEmacs-Beta@calypso.tux.org
http://calypso.tux.org/mailman/listinfo/xemacs-beta
